package br.pucrs.springmongodemo.demo.service;

import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.pucrs.springmongodemo.demo.model.Livro;
import br.pucrs.springmongodemo.demo.model.LivroRef;
import br.pucrs.springmongodemo.demo.model.User;
import br.pucrs.springmongodemo.demo.model.UserRef;
import br.pucrs.springmongodemo.demo.repository.LivroRefRepository;
import br.pucrs.springmongodemo.demo.repository.UserRefRepository;
import br.pucrs.springmongodemo.demo.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CriaELista {
    
    @Autowired
    private UserRepository repoUs;
    @Autowired
    private UserRefRepository repoUsr;
    @Autowired
    private LivroRefRepository repoLir;

    public void demo() {
        Livro l1 = new Livro("Meu Livro", "O grande autor");
        Livro l2 = new Livro("O Outro Livro", "O baita autor");

        LivroRef lr1 = new LivroRef(null, "Nosso Livro", "O pequeno autor");
        LivroRef lr2 = new LivroRef(null, "O Vosso Livro", "O menor autor");
        LivroRef liAux;

        User u1 = new User(null, "Maria", 20, new ArrayList<Livro>(Arrays.asList(l1)));
        UserRef ur1 = new UserRef(null, "Joana", 30, new ArrayList<String>());
        UserRef ur2 = new UserRef(null, "Paulo", 25, new ArrayList<String>());
        
        // **************************************
        repoUs.deleteAll();
    
        log.info("Antes do insert: " + u1.toString());
        repoUs.save(u1);
        log.info("Depois do insert: " + u1.toString());

        u1.addLivro(l2);
        log.info("Antes do save: " + u1.toString());
        repoUs.save(u1);
        log.info("Depois do save: " + u1.toString());

        // **************************************
        repoUsr.deleteAll();
        repoLir.deleteAll();

        repoLir.save(lr1);
        repoLir.save(lr2);

        liAux = repoLir.findByTitulo("Nosso Livro");
        ur1.addLivro(liAux.get_id());
        liAux = repoLir.findByTitulo("O Vosso Livro");
        ur1.addLivro(liAux.get_id());
        repoUsr.save(ur1);
        repoUsr.save(ur2);
    }

}
