package br.pucrs.springmongodemo.demo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.pucrs.springmongodemo.demo.model.User;

public interface UserRepository extends MongoRepository<User, String> {
    
}