package br.pucrs.springmongodemo.demo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.pucrs.springmongodemo.demo.model.LivroRef;

public interface LivroRefRepository extends MongoRepository<LivroRef, String> {
    public LivroRef findByTitulo(String titulo);
}