package br.pucrs.springmongodemo.demo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import br.pucrs.springmongodemo.demo.model.UserRef;

public interface UserRefRepository extends MongoRepository<UserRef, String> {
    
}