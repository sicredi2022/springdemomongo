package br.pucrs.springmongodemo.demo.model;


import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "livrosref")
public class LivroRef {
    private String _id;
    private String titulo;
    private String autores;
}
