package br.pucrs.springmongodemo.demo.model;


import java.util.List;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "users")
public class User {
    private String _id;
    private String nome;
    private int idade;
    private List<Livro> retirou;

    public void addLivro(Livro l){
        retirou.add(l);
    }
}
