package br.pucrs.springmongodemo.demo.model;


import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "livros")
public class Livro {
    private String titulo;
    private String autores;
}
